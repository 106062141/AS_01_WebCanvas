var canvas = document.getElementById("art");
var ctx = canvas.getContext("2d");

var clearCanvas;
var undo;
var redo;
var download;
var cStep=-1;
var cPushArray=new Array();
var start_pos;
var tool='pencil';
var dragging=false;
var startPos;
var pic = new Image();
var hasInput=false;
var texting=false;



window.addEventListener('load', init, false);
function init() {
    document.getElementById("clear").addEventListener('click',clear,false);
    
    document.getElementById("undo").addEventListener('click',cUndo,false);
    document.getElementById("redo").addEventListener('click',cRedo,false);
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    cStep=-1;
    cPush()
    ctx.lineWidth=4;
    document.getElementById("lineWidth").addEventListener("input", function(){ctx.lineWidth=this.value;}, false);
    document.getElementById('strokeColor').addEventListener("input", function(){ctx.strokeStyle=this.value;}, false);
    document.getElementById("download").addEventListener('click', savepng,false);
    //console.log(document.getElementById("fontselect").value);
    ctx.font = "40px"+" " +document.getElementById("fontselect").value;
    
}

function getMousePos(_canvas,evt) {
    var rect = _canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }

function savepng(){
    var link = document.createElement('a');
        link.href = canvas.toDataURL();
        link.download = "Untitled.png";
        link.click();
  }

function cPush() {
    
    cStep++;
    console.log(cStep);
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(canvas.toDataURL());
} 

function cUndo() {
    //console.log("dfghjkl");
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}


        
     

function clear() {
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    cStep=0;
    cPushArray.length=1;
    document.getElementById("lineWidth").value=4;
    ctx.lineWidth=4;
    document.getElementById("strokeColor").value="#000000";
    ctx.strokeStyle="#000000";
    tool='pencil';
    texting=false;
    document.getElementsByName("1")[0].checked=true;
}

function changetool(){
    dragging=false;
    if(document.getElementById("pencil").checked)
        tool='pencil';
    else if(document.getElementById("eraser").checked)
        tool='eraser';
    else if(document.getElementById("line").checked)
        tool='line';
    else if(document.getElementById("circle").checked)
        tool='circle'; 
    else if(document.getElementById("rectangle").checked)
        tool='rectangle';       
    else if(document.getElementById("triangle").checked)
        tool='triangle';
    else if(document.getElementById("type_mode").checked)
        tool='type_mode';      
    else 
        CSSConditionRule.log("changetool() have bug");

    if(tool==='type_mode') {
        texting=true;
        text_mode();
        //console.log(texting);
    }
    else texting=false;

}

function mouseMove(evt) {
    if (tool === "pencil"){
        canvas.style.cursor = "url(pencil.png) ,auto";
    }
    else if (tool === "eraser"){
        canvas.style.cursor = "url(eraser.png) ,auto";
    }
    else if (tool === "line"){
        canvas.style.cursor = "url(line.png) ,auto";
    }
    else if (tool === "circle"){
        canvas.style.cursor = "url(circle_cursor.png) ,auto";
    }
    else if (tool === "triangle"){
        canvas.style.cursor = "url(triangle_cursor.png) ,auto";
    }
    else if (tool === "rectangle"){
        canvas.style.cursor = "url(rectangle_cursor.png) ,auto";
    }
    else if(tool==="type_mode"){
        canvas.style.cursor = "crosshair";
    };
    if(dragging){
        if(tool==='eraser') ctx.strokeStyle='white';
        else if(tool==='pencil') ctx.strokeStyle=document.getElementById('strokeColor').value;
        if(tool==='eraser'||tool==='pencil'){
            var mousePos = getMousePos(canvas, evt); 
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
        }
        else if(tool==="line"){
            console.log("ss");
            ctx.strokeStyle=document.getElementById('strokeColor').value;
            ctx.beginPath();
            
            ctx.drawImage(pic, 0, 0);
            var pos=getMousePos(canvas,evt);
            ctx.moveTo(startPos.x,startPos.y);
            ctx.lineTo(pos.x, pos.y);
            ctx.stroke();
            //ctx.fill();
        }
        else if(tool==='circle'){
            ctx.strokeStyle=document.getElementById('strokeColor').value;
            ctx.beginPath();
            ctx.drawImage(pic, 0, 0);
            var pos=getMousePos(canvas,evt);
            ctx.moveTo(startPos.x, startPos.y);
            ctx.arc(startPos.x,startPos.y,distance(pos.x,pos.y),0,2*Math.PI);
            ctx.fillStyle=ctx.strokeStyle;
            ctx.fill();
        }
        else if(tool==='rectangle'){
            ctx.strokeStyle=document.getElementById('strokeColor').value;
            ctx.beginPath();
            ctx.drawImage(pic, 0, 0);

            var pos=getMousePos(canvas,evt);
            ctx.rect(startPos.x,startPos.y,pos.x-startPos.x, pos.y-startPos.y);
            ctx.fillStyle=ctx.strokeStyle;
            ctx.stroke();
            ctx.fill();//空心實心
            ctx.closePath();
            //ctx.stroke();
        }
        else if(tool==='triangle'){
            ctx.strokeStyle=document.getElementById('strokeColor').value;
            ctx.beginPath();
            ctx.drawImage(pic, 0, 0);

            var pos=getMousePos(canvas,evt);
            
            ctx.moveTo(startPos.x, startPos.y);
            ctx.lineTo(startPos.x-pos.x+startPos.x, startPos.y+pos.y-startPos.y);
            ctx.lineTo(startPos.x+pos.x-startPos.x, startPos.y+pos.y-startPos.y);
            //ctx.lineTo(startPos.x, startPos.y);
            ctx.fillStyle=ctx.strokeStyle;
            
            ctx.closePath();
            ctx.stroke();//跟下面的二擇一
            //ctx.fill();
            
        }
  }
}
  canvas.addEventListener('mousedown', function(evt) {
    dragging=true;
    startPos = getMousePos(canvas, evt);
    evt.preventDefault();
    pic.src=canvas.toDataURL();
    if(tool==='pencil'||tool==='eraser'){
        ctx.beginPath();
        ctx.moveTo(startPos.x, startPos.y); 
        
    }
 
    
  });

  canvas.addEventListener('mousemove', mouseMove, false);

  canvas.addEventListener('mouseup', function() {
      dragging=false;
      if(texting!=true){
        cPush();
      }
      
  }, false);


////////////////start upload
document.getElementById('file').addEventListener('change', upload_image, false);

function upload_image(evt){
    //console.log("ff");
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    //console.log(evt.target.files);
    reader.readAsDataURL(evt.target.files[0]);
    setTimeout(()=>{cPush()}, 10);
}
//////////////////////end upload


function text_mode() {
        var text_x, text_y;
  
        //console.log(texting);
        canvas.onclick = function (e) {
            if (hasInput) return;
            if(texting){
            text_x = e.offsetX;
            text_y = e.offsetY;
            addInput(e.clientX, e.clientY);
            }
        }
        function addInput(a, b) {
            var input = document.createElement('input');
            input.type = 'text';
            input.style.position = 'fixed';
            input.style.left = (a - 4) + 'px';
            input.style.top = (b - 4) + 'px';
            input.onkeydown = handleEnter;
            document.body.appendChild(input);
            input.focus();
            hasInput = true;
        }
        function handleEnter(e) {
            var keyCode = e.keyCode;
            if (keyCode === 13) {
                ctx.fillStyle='#000000';
                ctx.fillText(this.value, text_x - 5, text_y + 10);
                document.body.removeChild(this);
                hasInput = false;
                cPush();
            }
        }
        
}

function font_select(){
    ctx.font=document.getElementById("fontsize").value+"px"+" "+document.getElementById("fontselect").value;
}
/////////////////
function distance(a,b){
    var first=Math.pow(a-startPos.x,2);
    var second=Math.pow(b-startPos.y,2);
    var third=Math.sqrt(first+second);
    return third;
}
function loadImage(stepnow){
    var Image1 = new Image();
    Image1.src = url[stepnow];
    context.drawImage(Image1, 0, 0, 600, 600);
}